; ; 1
; MOV C, 5
; MOV D, 250
; CMP C, D

; ; 2
; MOV C, 5
; MOV D, 5
; CMP C, D

; ; 3
; MOV C, 5
; MOV D, 5
; AND D, C

; ; 4
; MOV C, 5
; MOV D, 2
; AND D, C

; ; 5
; MOV C, 5
; MOV D, 2
; OR D, C

; ; 6
; MOV C, 7
; MOV D, 2
; XOR D, C

; ; 7
; MOV D, 0
; NOT D

; 8
MOV D, 0xf0
NOT D

HLT