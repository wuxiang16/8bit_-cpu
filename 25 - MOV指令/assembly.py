import pin

FETCH = [
    pin.PC_OUT | pin.MAR_IN,
    pin.RAM_OUT | pin.IR_IN | pin.PC_INC,
    pin.PC_OUT | pin.MAR_IN,
    pin.RAM_OUT | pin.DST_IN | pin.PC_INC,
    pin.PC_OUT | pin.MAR_IN,
    pin.RAM_OUT | pin.SRC_IN | pin.PC_INC,
]

MOV = 0 | pin.ADDR2 # MOV指令定义为 1000 xxxx
ADD = (1 << pin.ADDR2_SHIFT) | pin.ADDR2 # ADD指令定义为 1001 xxxx

NOP = 0
HLT = 0x3f

INSTRUCTIONS = {
    2: { # 二地址指令
        MOV: { # MOV指令寻址方式
            (pin.AM_REG, pin.AM_INS): [ # (寄存器寻址，立即寻址) ==> MOV A,5
                pin.DST_W | pin.SRC_OUT, # 微指令：DST(A)寄存器写，SRC(5)寄存器输出
            ]
        }
    },
    1: {}, # 一地址指令
    0: { # 零地址指令
        NOP: [
            pin.CYC, # 让指令周期清零，跳过这次指令
        ],
        HLT: [
            pin.HLT, # 指令停止
        ]
    }
}

print(bin(MOV))