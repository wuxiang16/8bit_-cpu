; 1.
; MOV A, 5;

; 2.
; MOV A, B;

; 3.
; MOV A, [5];

; 4.
; MOV B, 6
; MOV A, [B]

; 5.
; MOV [0x2f], 5;

; 6. 
; MOV A, 0x18
; MOV [0x2f], A

; 7.
; MOV [0x2e], 0x18
; MOV [0x2f], [0x2e]

; 8. 
; MOV [0x18], 0xfe
; MOV A, 0x18
; MOV [0x2f], [A]

; 9.
; MOV A, 0x18
; MOV [A], 5

; 10.
; MOV A, 0x18
; MOV B, 0x33
; MOV [A], B

; 11. 
; MOV [0x30], 0xdd
; MOV A, 0x18
; MOV [A], [0x30]

; 12. 
MOV [0x30], 0xee
MOV B, 0x30
MOV A, 0x18
MOV [A], [B]

HLT;