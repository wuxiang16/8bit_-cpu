> ---- 整理自B站UP主 踌躇月光 的视频

# 1. Logic Circuit
[Logic Circuit 下载地址](https://www.logiccircuit.org/download.html)

- 界面如下，实际使用可下载体验

![alt text](image.png)

# 2. 与或非门
![alt text](image-1.png)
![alt text](image-2.png)
![alt text](image-3.png)
# 3. 实验
[【00 - Logic Circuit 简介 -- 与或非门】](https://gitee.com/wuxiang16/8bit_-cpu/tree/master/00%20-%20Logic%20Circuit%20%E7%AE%80%E4%BB%8B%20--%20%E4%B8%8E%E6%88%96%E9%9D%9E%E9%97%A8)
